# frozen_string_literal: true

require File.expand_path('lib/fun_translations/version', __dir__)

Gem::Specification.new do |spec|
  spec.name                  = 'fun_translations'
  spec.version               = FunTranslations::VERSION
  spec.authors               = ['First name ']
  spec.email                 = ['example@gmail.com']
  spec.summary               = ''
  spec.description           = ''
  spec.homepage              = 'https://github.com'
  spec.license               = 'MIT'
  spec.platform              = Gem::Platform::RUBY
  spec.required_ruby_version = '>= 3.1.0'

  spec.files = Dir['lib/**/*.rb',
                   'fun_translations.gemspec', '.github/*.md',
                   'Gemfile', 'Rakefile']
  # spec.extra_rdoc_files = ['README.md']
  spec.require_paths = ['lib']

  spec.add_dependency 'faraday',                '~> 2.6'
  spec.add_dependency 'zeitwerk',               '~> 2.4'

  spec.add_development_dependency 'pry',        '~> 0.14'
  spec.add_development_dependency 'pry-byebug', '~> 3.10'
  spec.add_development_dependency 'rspec',      '~> 3.6'
  spec.add_development_dependency 'simplecov',  '~> 0.16'
  spec.add_development_dependency 'webmock',    '~> 3.14'

  spec.metadata = {
    'rubygems_mfa_required' => 'true'
  }
end
