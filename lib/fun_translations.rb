# frozen_string_literal: true

require 'faraday'
require 'json'
require 'zeitwerk'
require 'pry'
require 'pry-byebug'

loader = Zeitwerk::Loader.for_gem
loader.setup

module FunTranslations
  def self.client(token = nil)
    FunTranslations::Client.new(token)
  end
end

# require 'fun_translation'

# client = FanTranslation.client('23123123123') #::Client.new
# result = client.translate :pirate, 'Hello sir!'
# puts result.translated_text
