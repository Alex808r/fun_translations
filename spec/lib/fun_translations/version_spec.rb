# frozen_string_literal: true

RSpec.describe FunTranslations do
  it 'constant version string' do
    expect(described_class::VERSION).to be_an_instance_of(String)
  end
end
